"use strict";
class RecruitmentAgency {
    constructor(programmers = [], programmer = {
        name: "",
        framework: "",
        experience: 0,
        available: false
    }) {
        this.urlApi = "https://files.gwo.pl/custom/random-data.json";
        this.index = 0;
        this.request = async () => {
            const response = await fetch(this.urlApi);
            let tempArray = await response.json();
            this.setIndex(tempArray);
        };
        this.name = programmer.name;
        this.framework = programmer.framework;
        this.experience = programmer.experience;
        this.available = programmer.available;
        this.programList = [];
        this.request();
        this.setIndex(programmers);
    }
    setIndex(programmers) {
        for (let index = 0; index < programmers.length; index++) {
            this.programList.push(Object.assign({}, programmers[index], { id: this.index++ }));
        }
    }
    fillArray() {
        const programmers = [
            {
                name: "Kazimierz",
                framework: "Angular",
                experience: 4,
                available: true
            },
            {
                name: "Włodzimierz",
                framework: "React",
                experience: 2,
                available: true
            },
            {
                name: "Zbigniew",
                framework: "Vue.js",
                experience: 1,
                available: false
            }
        ];
        return programmers;
    }
    addProgrammer(addProgrammer) {
        addProgrammer = Object.assign({}, addProgrammer, { id: this.index++ });
        this.programList.push(addProgrammer);
        return JSON.stringify(this.programList);
    }
    deleteProgrammer(deleteProgrammer) {
        let index = this.programList.indexOf(deleteProgrammer);
        if (index === -1) {
            console.error("Nie znaleziono takiego programisty");
            return;
        }
        this.programList.splice(index, 1);
        return JSON.stringify(this.programList);
    }
    getAllProgrammers() {
        // let tempArray: string[] = [];
        // this.programList.forEach(element => {
        //   tempArray.push(
        //     " Imię: " +
        //       element.name +
        //       " Framework: " +
        //       element.framework +
        //       " Doświadczenie: " +
        //       element.experience +
        //       " Dostępność: " +
        //       element.available
        //   );
        // });
        // return tempArray.join("<br>");
        return this.programList;
    }
    updateProgrammer(updateProgrammer) {
        if (updateProgrammer.hasOwnProperty("id")) {
            this.programList[this.programList.findIndex((el) => el.id === updateProgrammer.id)] = updateProgrammer;
            return JSON.stringify(this.programList);
        }
        else {
            console.log("Nie ma takiego programisty w bazie danych");
        }
    }
    getFilteredProgrammers(itemList, searchKeyword) {
        if (!itemList)
            return [];
        if (!searchKeyword)
            return itemList;
        this.programList = [];
        if (itemList.length > 0) {
            searchKeyword = searchKeyword.toLowerCase();
            itemList.forEach((item) => {
                //Object.values(item) => gives the list of all the property values of the 'item' object
                let propValueList = Object.values(item);
                for (let i = 0; i < propValueList.length; i++) {
                    if (propValueList[i]) {
                        if (propValueList[i]
                            .toString()
                            .toLowerCase()
                            .indexOf(searchKeyword) > -1) {
                            this.programList.push(item);
                            break;
                        }
                    }
                }
            });
        }
        return JSON.stringify(this.programList);
    }
    getShowcase() {
        let tempArray = ["Imiona zarejestrowanych programistów:\n"];
        this.programList.forEach(element => {
            tempArray.push(element.name + "\n");
        });
        return tempArray.join("<br>");
    }
}
let recruitmentAgency = new RecruitmentAgency();
window.onload = () => {
    setTimeout(() => {
        let allProgrammersJSON = JSON.stringify(recruitmentAgency.getAllProgrammers());
        document.getElementById("allProgrammers").innerHTML = allProgrammersJSON;
        let welcomeInfo = recruitmentAgency.getShowcase();
        document.getElementById("helloText").innerHTML = welcomeInfo;
        let addProgrammer = recruitmentAgency.addProgrammer(recruitmentAgency.fillArray()[0]);
        document.getElementById("addingProgrammer").innerHTML = addProgrammer;
        let removeProgrammer = recruitmentAgency.deleteProgrammer(recruitmentAgency.getAllProgrammers()[6]);
        document.getElementById("removingProgrammer").innerHTML = removeProgrammer;
        let updateProgrammer = recruitmentAgency.getAllProgrammers()[5];
        updateProgrammer.name = "Wojtek";
        let test = recruitmentAgency.updateProgrammer(updateProgrammer);
        document.getElementById("editingProgrammer").innerHTML = test;
        let filterProgrammer = recruitmentAgency.getFilteredProgrammers(recruitmentAgency.getAllProgrammers(), "Angular");
        document.getElementById("filteringProgrammers").innerHTML = filterProgrammer;
    }, 200);
};
// let loadedData = recruitmentAgency.getAllProgrammers();
// console.log(loadedData);
// document.body.innerHTML = JSON.stringify(loadedData);
// recruitmentAgency.addProgrammer(recruitmentAgency.fillArray()[0]);
// recruitmentAgency.addProgrammer(recruitmentAgency.fillArray()[1]);
// recruitmentAgency.addProgrammer(recruitmentAgency.fillArray()[2]);
// recruitmentAgency.deleteProgrammer(recruitmentAgency.getAllProgrammers()[0]);
// let updateProgrammer = recruitmentAgency.getAllProgrammers()[0];
// updateProgrammer.name = "Wojtek";
// recruitmentAgency.updateProgrammer(updateProgrammer);
// console.log(recruitmentAgency.getShowcase());
